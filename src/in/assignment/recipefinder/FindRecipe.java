package in.assignment.recipefinder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import in.assignment.recipefinder.models.Ingredient;
import in.assignment.recipefinder.models.Item;
import in.assignment.recipefinder.models.Recipe;
import in.assignment.recipefinder.models.Unit;
import in.assignment.recipefinder.utility.TimeVariant;


public class FindRecipe {

	
	 
	public static void main(String[] args){
		
		  if (args == null || args.length != 2) {
	            System.out.println("Please enter both FridgeItem CSV and Recipe JSON file paths");
	            System.exit(0);
	        }
		  
		 
			FindRecipe findRecipe = new FindRecipe();
			System.out.println(findRecipe.getRecipeWrapper(args[0],args[1]));
			
			
		   // System.out.println(findRecipe.getRecipeWrapper("/Users/Junaidmasood/Desktop/fridge.csv","/Users/Junaidmasood/Desktop/recipe.json"));
		   
		   
		 
	}
	
	//Wrapper function to get the Today's Recipe
	
	public String getRecipeWrapper(String itemsPath, String recipePath){
		
		return getRecipe(readCSV(itemsPath),readJSON(recipePath));
		
	}
	
	// Function to get the Today's Recipe
	
	public String getRecipe(HashMap<String, Item> items, List<Recipe> recipes){
	
		
		Recipe answer=null;
		Date date = new Date(Long.MAX_VALUE);
		
		for(Recipe recipe : recipes){
			
			if(isPossible(items,recipe))
			{
				Date latestDate = getLatestIngredientDate(items,recipe);
				if(latestDate!=null&&latestDate.before(date))
					{
					date = latestDate;
					answer = recipe;
					}
			}
			
			}
		if(answer!=null)
		   return answer.getName();
		else
		   return "Order Takeout";
		
		
	}
	
	// Helper method to check if a Recipe can be made from the available Items
	
	 public boolean isPossible(HashMap<String, Item> items, Recipe recipe){
		
		
		boolean possible = false;
		
		for(Ingredient ingredient : recipe.getIngredients()){
			String name = ingredient.getName();
			// Check if ingredient is in the Fridge and it is not expired and has the required amount
			if(		(items.containsKey(name))&&
					(items.get(name).getUseBy().after(TimeVariant.newDate()))&&
					(items.get(name).getAmount()>=ingredient.getAmount())){
	
				possible = true;
				
			}
			else return false;
		}
		
		return possible;
		
	}
	 
	 // Helper method to get the closest UseBy date of ingredient from all the ingredients of a Recipe
	 
	 public Date getLatestIngredientDate(HashMap<String, Item> items, Recipe recipe){
			

			Date date = new Date(Long.MAX_VALUE);
			
			for(Ingredient ingredient : recipe.getIngredients()){
				String name = ingredient.getName();
				if(		(items.containsKey(name))&&
						(items.get(name).getUseBy().after(TimeVariant.newDate()))&&
						(items.get(name).getAmount()>=ingredient.getAmount())){
					  
					if(items.get(name).getUseBy().before(date))
						date = items.get(name).getUseBy();
					
				}
				else return null;
			}
			
			return date;
			
		}
	
	//Method to read Recipe's JSON 
	
	public List<Recipe> readJSON(String recipePath){
		
		
		String jsonString=null;
		try {
			jsonString = FileUtils.readFileToString(new File(recipePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Gson gson = new Gson();

		@SuppressWarnings("serial")
		Type collectionType = new TypeToken<List<Recipe>>() {
		}.getType();
		
		// Deserialize the json array to form an array of java Recipe objects
		List<Recipe> recipes = gson.fromJson(jsonString, collectionType);
		
		return recipes;


	}
	
	//Method to read Fridge's Item CSV 
	
    public HashMap<String,Item> readCSV(String itemsPath){
    	

    	BufferedReader br = null;
    	String line = "";
    	String cvsSplitBy = ",";
    	HashMap<String,Item> items=new HashMap<String,Item>();
    	try {
    		

    		br = new BufferedReader(new FileReader(itemsPath));
    		while ((line = br.readLine()) != null) {

    			String[] item = line.split(cvsSplitBy);
    			String name = item[0];
    			int amount = Integer.parseInt(item[1]);
    			Unit unit = Unit.valueOf(item[2]);
    			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    			String dateInString = item[3]; 
    			Date useBy=null;
    			try {
					useBy = formatter.parse(dateInString);
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		
    			items.put(name, new Item(name,amount,unit,useBy));

    		}

    	} catch (FileNotFoundException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		if (br != null) {
    			try {
    				br.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	
    	return items;
    }
    
    
    
    
}
