# README #

To get a working jar from the project goto target and copy the generated file named 
"recipes-0.0.1-SNAPSHOT-jar-with-dependencies.jar" and run it from command prompt with:
$java -jar <jar-name>.jar <fridge-item-path>/<fridge-csv-file>.csv <recipe-item-path>/<recipe-json-file>.json

Sample csv and json files could be found in the resources folder.